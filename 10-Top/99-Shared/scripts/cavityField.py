# Embedded python script
from epics import caget, caput
import array, time


arr_time= array.array('d', [1, 2, 3, 4, 5, 6, 7, 8, 9] )
caput("DTL-010::CavFieldWf-XAxis", arr_time)

while True:

	val0=caget("DTL-010:RFS-DIG-102:AI4-SMonAvg-Mag")
	val1=caget("DTL-010:RFS-DIG-102:AI5-SMonAvg-Mag")
	val2=caget("DTL-010:RFS-DIG-102:AI6-SMonAvg-Mag")
	val3=caget("DTL-010:RFS-DIG-102:AI7-SMonAvg-Mag")
	val4=caget("DTL-010:RFS-DIG-101:AI0-SMonAvg-Mag")
	val5=caget("DTL-010:RFS-DIG-103:AI0-SMonAvg-Mag")
	val6=caget("DTL-010:RFS-DIG-103:AI1-SMonAvg-Mag")
	val7=caget("DTL-010:RFS-DIG-103:AI2-SMonAvg-Mag")
	val8=caget("DTL-010:RFS-DIG-103:AI3-SMonAvg-Mag")

	arr_wf= array.array('d', [val0, val1, val2, val3, val4, val5, val6, val7, val8] )

	#print(arr_wf)
	#print(arr_time)
	caput("DTL-010::CavFieldWf", arr_wf)

	time.sleep(1)

